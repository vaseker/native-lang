# Местоимения
ich – я

du – ты

er – он
sie – она
es – оно

wir – мы
ihr – вы

sie – они
Sie – Вы

# Глаголы

## Sein – быть

ich bin

du bist

er/sie/es ist

wir sind

iht seid

sie/Sie sind

## Haben – иметь
ich habe

du hast

er/sie/es hat

wir haben

ihr habt

sie/Sie haben

## Другие глаголы (окончания)

ich [e]

du [st]

er/sie/es [t]

wir [en]

ihr [t]

sie/Sie [en]
___

wohnen – жить

leben  – жить

trinken – пить

nehmen – брать

geboren – рожден

# Рода

Feminin [die] – женский

Maskulin [der] – мужской

Neutrum [das] – средний

# Артикли
| Biespiele                   | Feminin | Muskulin | Neutrum | Plural |
|-----------------------------|---------|----------|---------|--------|
| der bestimmte Artikel       | die     | der      | das     | die    |
| der unbestimmte Artikel (+) | eine    | ein      | ein     | –      |
| der negative Artikel (–)    | keine   | kein     | kein    | keine  |

# Ед/мн. числа
| Singular | Plural            |
|----------|-------------------|
| 1 Dialog | 2, 3, ... Dialoge |
| 1 Bild   | 2, 3, ... Bilder  |
| 1 Zahl   | 2, 3, ... Zahlen  |

# Вопросы

wie – как

was – что

woher – откуда

wohin – куда

wo – где

wer – кто

warum – почему

welche – какой

wann – когда

wann - кто

wie lange - как долго

### Wie heißt *das* auf Deutsch? – Как *это* называется на Немецком?
> Das heißt *xxx* auf Deutsch. – Это назвается *ххх* на Немецком.

### Wo arbeiten du? – Где ты работаешь?
> Bei Yandex. – В Яндексе.

### Wo wohnst du?
> Где ты живешь?

### Woher kommt Laura?
> Откуда Лаура?

### Wie heißen Sie?
> Как Вас зовут?

### Trinkst du Kaffee?
> Ты пьешь кофе?

### Haben Sie Kinder?
> У Вас есть дети?

### Kristian ist nicht verhairaitet.
> Кристиан не женат.

# Числа
0 – null

1 – ein

2 – zwei

3 – drei

4 – vier

5 – fünf

6 – sechs

7 – sieben

8 – acht

9 – neun

10 – zehn

11 – elf

12 – zwölf

13 – drei[zehn]

16 – sechzehn

17 – siebzehn

20 – zwanzig

21 – einundzwanzig

30 – dreißig

60 – sechzig

70 – siebzig

100 – (ein)hundert

125 – einhundertfünfundzwanzig

1 000 - (ein)tausend

12 110 - zwölftausendeinhundertzehn

100 000 - (ein)hunderttausend

253000 - zweihundertdreiundfünfzigtausend

1 000 000 - eine Million

6 500 000 - sechs Millionen fünfhunderttausend

1000 Millionen - eine Milliarde

# Алфавит

**A** – **B**e – **C**e – **D**e – **E** – e**F** – **G**e – **H**a – **I** – **J**ot – **K**a – e**L** – e**M** – e**N** – **O** – **P**e – **Q**u – e**R** – e**S** – **T**e – **U** – **V**au – **W**e – i**X** – **Y**psilon – **Z**et

# Слова

die Vorwal – телефонный код

Ich weiß nicht – я не знаю

Staatsangehörigkeit – гражданство

. – punkt – точка

@ – affenschwanz – собака

\– – bindestrich

\_ – unterstrich

Bitte langsam – помедленнее, пожалуйста

Buchstabieren – по буквам

### Wie schreibt man das? – Как это пишется?

Ich möchte die Nummer von Filipe Rodriguez. – Я бы хотел узнать номер Филипа Родригеза.

ledig – холостой

verheiratet – женатый

geschieden – разведенный

die Postleitzahl – почтовый индекс

Zettel – записка

gleich – скоро

überall – повсюду

helfen – помогать

keinen – несколько

geben – давать

essen – есть

heute – сегодня

klingeln – звонить (в звонок)

Tür – дверь

gehen – идти

besuchen – навещать

öffnen – открывать

schwer – тяжелый

sehr – довольно–таки

### Задачи, упражнения

das Bild – картинка

der Dialog – диалог

die Regel – правило

das Formular – анкета

der Rap – рэпчик

der Lesertext – текст для чтения

das Lied – песня

die Liste – список

die Tabelle – таблица

die Zahlen – цифры

### Жратва

der Tee – чай

das Käsebrot – бутерброд с сыром
 
der Rotwein – красное вино

der Weißwein – белое вино

das Mineralwasser – минералка

die Cola – Кола

der Kuchen – пирог

das Würstchen – сосиска

die Gulaschsuppe – суп–гуляш

der Salat – салат

der Kaffee – кофе

das Schinkenbrot – бутерброд с колбасой

das Hähnchen – курица

das Bier – пивасик

das Ei – яйцо

das Eis - мороженное

der Orangensaft – апельсиновый сок

### Слова

### Nomen - существительные

das Alter - возраст

der Anfang - начало

die/der Angestelle - служащий

die Anmeldung - сообщение

der Apfelsaft - яблочный сок

der Appetit - аппетит

die Auskunft - информация, справка

der Ausweis - удостоверение

das Auto - автомобиль

der Besuch - визит

der Buchstrabe - буква

der Dank - благодарность

der Familienstand - семейное положение

der Fehler - ошибка

der Fernseher - телезритель

die Firma - фирма

das Formular - формуляр

der Führerschein - водительские права

das Geburtsjahr - год рождения

der Gebursort - место рождения

das Haus - дом

die Kollegin - коллега

die Lampe - лампа

der Mantel - пальто

die Methode - метод

die Milch - молоко

der Monat - месяц

der Nachbar - сосед

der Nachmittag - послеобеденное время

das Paar - несколько

die Portion - порция

die Religion - религия

der Salat - салат

die Seite - сторона

die Situation - ситуация

die Speisekarte - меню

die Straße - улица

das Stüсk - штука, единица

der Student - студент

die Suppe - суп

die Tasse - чашка

die Tür - дверь

das Wasser - вода

der Wein - вино

die Woche - неделя

die Wohnung - квартира, жильё

das Zimmer - комната

das Zentrum - центр

### Verben - глаголы

begrüßen - приветствие

bestellen - заказывать

besuchen - навещать

buchstabieren - по буквам

essen - есть

fehlen - отсутствовать

gibt -> geben - давать

geborn sein - уроженец

gehen - идти

glauben - думать

haben - иметь

hilft -> helfen - помогать

klingeln - звонить в дверной звонок

lädt ... ein (-> einladen) - приглашать

nehmen - брать

öffnen - открывать

stehen - стоять

trinken - пить

weiß (-> wissen) - знать

wiederholen - повторять

### Adjektive - прилагательные

alt - взрослый

deutlich - ясный

freundlich - дружелюбный

hübsch - миленький

international - интернациональный

klein - маленький

langsam - медленный

laut - громкий

männlich - мужской

neu - новый

unfreundlich - недружелюбный

weiblich - женский

wunderbar - восхитительный

### Еще словечки

also - следовательно

an - около

halt - стоп

in der Mitte - посередине

gern(e) - охотно

gleich - скоро

jemand - кто-то

links - слева

meist- - часто

natürlich - настоящий

nur - только

oben - наверно

rechts - справа

untetn - снизу

vielleicht - возможно

wirklich - действительно

zweimal - два раза

wechselt - менять

Suche - поиск

Bahnhof - вокзал

jede Woche - каждую неделю

### Möbel - Мебель

die Küche - кухня

die Lampe - лампа

der Stuhl - стул

der Teppich - ковер

der Schrank - шкаф

der Tisch - стол

der Sessel - кресло

das Sofa - диван

das Bett - кровать

das Regal - полка

schreiben - письменный

hoch - высокий

das Kleid - платье

kleider - платяной

garten - садовый

einbau - встроенный

altmodisch - старомодный

modern - современный

sehr günstig - очень выгодно

bequem - удобный

nicht billig - не дешевый

ganz hübsch - довольно милый

nicht schlecht - не плохой

unbequem - неудобный

günstig - выгодный

interessant - интересный

zu teuer - слишком дорогой

langweilig - скучный

groß - большой

Große - размер

Einwohner - население

Währung - валюта

die Hauptstadt - столица

große Städte - крупный город

### Adjectives - окраска прилагательных
#### Положительно - нейтрально - негативно

| sehr gut     | neutral        | nicht gut   |
|--------------|----------------|-------------|
| toll         | bequem         | zu teuer    |
| sehr günstig | pracktisch     | nicht bilig |
| sehr schick  | ganz schön     | nicht schön |
|              | interessant    | zu gross    |
|              | nicht schlecht |             |

### Akkusativ - родительный падеж

|           | f           | m              | n         | Plural       |
|-----------|-------------|----------------|-----------|--------------|
| Nominativ | die Lampe   | der Teppich    | das Sofa  | die Stühle   |
|           | eine Lampe  | ein Teppich    | ein Sofa  | – Stühle     |
|           | keine Lampe | kein Teppich   | kein Sofa | keine Stühle |
| Akkusativ | die Lampe   | den Teppich    | das Sofa  | die Stühle   |
|           | eine Lampe  | einen Teppich  | ein Sofa  | – Stühle     |
|           | keine Lampe | keinen Teppich | kein Sofa |              |
